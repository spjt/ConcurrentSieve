(ns primes
 (:gen-class))

(use 'clojure.set)

(def prime-max (int (Math/pow 10 7)))
(def prime-min 3)

;; short: The set of primes from 1 to sqrt(prime-max)
(def short-max (int (+ 1 (Math/sqrt prime-max)))) 
(def short-list (range prime-min (inc short-max) 2))
(def short-set (set short-list))
(def short-set-agent (agent short-set))

(defn gen-rem-list [x mx]
  "Generate a list of multiples of x from x+1 to mx+1 (ie not including x).
   These will be non-prime."
  (rest (range x mx x)))

(defn gen-primes [a]
  "Fill the short-set-agent with the short primes set."
  (dotimes [x short-max]
    (if (and (> x 1) (contains? @a x))
      (send a #(difference % (gen-rem-list x short-max)))
      nil)))

;; prime-list-agent: Will eventually contain all primes not in short set.
(def prime-list-agent (agent ()))
(def partition-size 100000)
(def partition-count (/ prime-max partition-size))

(defn sieve-segment-x [s x]
  "Remove all multiples of x from set s"
  (remove #(zero? (mod % x)) s))

(defn sieve-segment [s rs]
  "Remove all multiples of all elements of rs from set s"
  (if (empty? rs) s
      (sieve-segment (sieve-segment-x s (first rs)) (rest rs))))

(defn sieve-all-segments []
  "Concurrently run sieve on all partitions." 
  (dotimes [x partition-count]
    (let [pt (range (inc (* partition-size x)) (* partition-size (inc x)) 2)]
      (send prime-list-agent conj (sieve-segment pt @short-set-agent)))))

(defn get-flat-list []
  (let [short-list (conj (remove #(= % 1) (into () @short-set-agent)) 2)
        prime-list @prime-list-agent]
    (flatten (conj prime-list short-list))))

(defn run []
  (do
    (gen-primes short-set-agent)
    (await short-set-agent)
    (sieve-all-segments)
    (await prime-list-agent)
    (spit "p1.out" (str "Primes: " (apply list (get-flat-list))))
    (shutdown-agents)))

(defn -main [] (run))

(run)